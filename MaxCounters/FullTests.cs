﻿using NUnit.Framework;

namespace MaxCounters
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(5, new[] {3, 4, 4, 6, 1, 4, 4}, new[] {3, 2, 2, 4, 2});
        }

        private void Test(int N, int[] A, int[] expected)
        {
            var solution = new Solution();

            var actual = solution.solution(N, A);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
