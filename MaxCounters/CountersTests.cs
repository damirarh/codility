﻿using NUnit.Framework;

namespace MaxCounters
{
    [TestFixture]
    public class CountersTests
    {
        [Test]
        public void Initialization()
        {
            var counters = new Counters(4);

            var actual = counters.GetValues();
            var expected = new int[] {0, 0, 0, 0};
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void IncreaseCounters()
        {
            var counters = new Counters(4);

            counters.ExecuteCommand(1);
            counters.ExecuteCommand(3);
            counters.ExecuteCommand(1);

            var actual = counters.GetValues();
            var expected = new int[] {2, 0, 1, 0};
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void ExecuteMaxCounter()
        {
            var counters = new Counters(4);
            counters.ExecuteCommand(1);
            counters.ExecuteCommand(3);
            counters.ExecuteCommand(1);

            counters.ExecuteCommand(5);
            
            var actual = counters.GetValues();
            var expected = new int[] {2, 2, 2, 2};
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void IncreaseAfterMaxCounter()
        {
            var counters = new Counters(4);
            counters.ExecuteCommand(1);
            counters.ExecuteCommand(3);
            counters.ExecuteCommand(1);
            counters.ExecuteCommand(5);

            counters.ExecuteCommand(2);
            
            var actual = counters.GetValues();
            var expected = new int[] {2, 3, 2, 2};
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}