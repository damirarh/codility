﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/max_counters
namespace MaxCounters
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int[] solution(int N, int[] A)
        // ReSharper restore InconsistentNaming
        {
            var counters = new Counters(N);
            foreach (var command in A)
            {
                counters.ExecuteCommand(command);
            }
            return counters.GetValues();
        }
    }
}