﻿using System;
using System.Collections;

namespace MaxCounters
{
    public class Counters
    {
        private readonly int[] _counters;
        private readonly int _maxCounterCommand;
        private readonly BitArray _countersNotAtCommonValue;
        private int _maxCounterValue = 0;
        private int _commonCounterValue = 0;

        public Counters(int count)
        {
            _counters = new int[count];
            _countersNotAtCommonValue = new BitArray(count);
            _maxCounterCommand = _counters.Length + 1;
        }

        public int[] GetValues()
        {
            var output = new int[_counters.Length];
            for (int i = 0; i < _counters.Length; i++)
            {
                if (_countersNotAtCommonValue[i])
                {
                    output[i] = _counters[i];
                }
                else
                {
                    output[i] = _commonCounterValue;
                }
            }
            return output;
        }

        public void ExecuteCommand(int command)
        {
            if (command == _maxCounterCommand)
            {
                _countersNotAtCommonValue.SetAll(false);
                _commonCounterValue = _maxCounterValue;
            }
            else
            {
                var index = command - 1; // array is 0-based, counter indices are 1-based
                if (_countersNotAtCommonValue[index])
                {
                    _counters[index]++;
                }
                else
                {
                    _counters[index] = _commonCounterValue + 1;
                }
                _countersNotAtCommonValue[index] = true;
                if (_counters[index] > _maxCounterValue)
                {
                    _maxCounterValue = _counters[index];
                }
            }
        }
    }
}