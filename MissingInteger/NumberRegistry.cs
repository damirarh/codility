﻿using System.Collections;

namespace MissingInteger
{
    public class NumberRegistry
    {
        private readonly BitArray _registry;

        public NumberRegistry(int size)
        {
            _registry = new BitArray(size);
        }

        public int GetFirstMissing()
        {
            for (int index = 0; index < _registry.Length; index++)
            {
                if (!_registry[index])
                {
                    return index + 1; // numbers are 1 based, not 0 based
                }
            }
            return _registry.Length + 1;
        }

        public void Register(int number)
        {
            // skip negative numbers, because we're looking for the smallest positive integer
            // skip number largen than array size, because a smaller number will be missing for certain
            if ((number > 0) && (number <= _registry.Length))
            {
                _registry[number - 1] = true;
            }
        }
    }
}