﻿using NUnit.Framework;

namespace MissingInteger
{
    [TestFixture]
    public class NumberRegistryTests
    {
        [Test]
        public void Initialize()
        {
            var registry = new NumberRegistry(5);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }

        [Test]
        public void AddFirstNumber()
        {
            var registry = new NumberRegistry(5);

            registry.Register(1);

            Assert.AreEqual(2, registry.GetFirstMissing());
        }

        [Test]
        public void AddNonFirstNumber()
        {
            var registry = new NumberRegistry(5);

            registry.Register(2);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }

        [Test]
        public void AddDuplicateNumber()
        {
            var registry = new NumberRegistry(5);
            registry.Register(2);

            registry.Register(2);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }

        [Test]
        public void AddNegativeNumber()
        {
            var registry = new NumberRegistry(5);

            registry.Register(-2);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }

        [Test]
        public void AddZero()
        {
            var registry = new NumberRegistry(5);

            registry.Register(0);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }

        [Test]
        public void AddOutOfBoundsNumber()
        {
            var registry = new NumberRegistry(5);

            registry.Register(6);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }

        [Test]
        public void AddMaximumNumber()
        {
            var registry = new NumberRegistry(5);

            registry.Register(5);

            Assert.AreEqual(1, registry.GetFirstMissing());
        }
    }
}