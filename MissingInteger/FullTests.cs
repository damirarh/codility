﻿using System.Linq;
using NUnit.Framework;

namespace MissingInteger
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new[] {1, 3, 6, 4, 1, 2}, 5);
        }

        [Test]
        public void NegativeNumbers()
        {
            Test(new[] {1, 3, 6, 4, -2, 2}, 5);
        }

        [Test]
        public void OutOfBoundsNumbers()
        {
            Test(new[] {1, 3, 6, 4, 1000000, 2}, 5);
        }

        [Test]
        public void DuplicateNumbers()
        {
            Test(new[] {1, 3, 6, 1, 3, 2}, 4);
        }

        [Test]
        public void FullRange()
        {
            Test(Enumerable.Range(1, 100).ToArray(), 101);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
