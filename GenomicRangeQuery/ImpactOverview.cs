﻿using System;
using System.Collections.Generic;

namespace GenomicRangeQuery
{
    public class ImpactOverview
    {
        private readonly string _nucleotides;
        private readonly int[,] _impactOffsets;

        private readonly Dictionary<char, int> _impacts = new Dictionary<char, int>
        {
            {'A', 1},
            {'C', 2},
            {'G', 3},
            {'T', 4},
        };

        public ImpactOverview(string nucleotides)
        {
            _nucleotides = nucleotides;
            _impactOffsets = new int[_nucleotides.Length, 4];
        }

        public int NextIndexToProcess { get; private set; }
        public bool DoneProcessing => NextIndexToProcess >= _nucleotides.Length;

        public int[] GetImpactOffsets(int index)
        {
            return new[]
            {
                _impactOffsets[index, 0],
                _impactOffsets[index, 1],
                _impactOffsets[index, 2],
                _impactOffsets[index, 3]
            };
        }

        public void ProcessNext()
        {
            var impact = _impacts[_nucleotides[NextIndexToProcess]];

            for (int i = 0; i + 1 < impact; i++)
            {
                _impactOffsets[NextIndexToProcess, i] = Int32.MaxValue;
            }

            var offset = 1;
            while ((NextIndexToProcess - offset >= 0) && 
                (_impactOffsets[NextIndexToProcess - offset, impact - 1] > offset))
            {
                for (int i = impact - 1; i < 4; i++)
                {
                    if (_impactOffsets[NextIndexToProcess - offset, i] > offset)
                    {
                        _impactOffsets[NextIndexToProcess - offset, i] = offset;
                    }
                }
                offset++;
            }

            NextIndexToProcess++;
        }
    }
}