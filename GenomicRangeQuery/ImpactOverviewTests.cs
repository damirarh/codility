﻿using System;
using NUnit.Framework;

namespace GenomicRangeQuery
{
    [TestFixture]
    public class ImpactOverviewTests
    {
        [Test]
        public void Initialization()
        {
            var impacts = new ImpactOverview("C");

            Assert.AreEqual(0, impacts.NextIndexToProcess);
            Assert.IsFalse(impacts.DoneProcessing);
        }

        [Test]
        public void ImpactSingleA()
        {
            var impacts = new ImpactOverview("A");

            impacts.ProcessNext();

            Assert.AreEqual(1, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {0, 0, 0, 0}, impacts.GetImpactOffsets(0));
        }

        [Test]
        public void ImpactSingleC()
        {
            var impacts = new ImpactOverview("C");

            impacts.ProcessNext();

            Assert.AreEqual(1, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, 0, 0, 0}, impacts.GetImpactOffsets(0));
        }

        [Test]
        public void ImpactSingleG()
        {
            var impacts = new ImpactOverview("G");

            impacts.ProcessNext();

            Assert.AreEqual(1, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, Int32.MaxValue, 0, 0}, impacts.GetImpactOffsets(0));
        }

        [Test]
        public void ImpactSingleT()
        {
            var impacts = new ImpactOverview("T");

            impacts.ProcessNext();

            Assert.AreEqual(1, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, Int32.MaxValue, Int32.MaxValue, 0}, impacts.GetImpactOffsets(0));
        }

        [Test]
        public void NextNucleotideWithGreaterImpact()
        {
            var impacts = new ImpactOverview("TC");
            impacts.ProcessNext();

            impacts.ProcessNext();

            Assert.AreEqual(2, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, 1, 1, 0}, impacts.GetImpactOffsets(0));
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, 0, 0, 0}, impacts.GetImpactOffsets(1));
        }

        [Test]
        public void NextNucleotideWithSmallerImpact()
        {
            var impacts = new ImpactOverview("CT");
            impacts.ProcessNext();

            impacts.ProcessNext();

            Assert.AreEqual(2, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, 0, 0, 0}, impacts.GetImpactOffsets(0));
            CollectionAssert.AreEqual(new[] {Int32.MaxValue, Int32.MaxValue, Int32.MaxValue, 0}, impacts.GetImpactOffsets(1));
        }

        [Test]
        public void BackProcessingHitsLowerOffset()
        {
            var impacts = new ImpactOverview("ACA");
            impacts.ProcessNext();
            impacts.ProcessNext();

            impacts.ProcessNext();

            Assert.AreEqual(3, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {0, 0, 0, 0}, impacts.GetImpactOffsets(0));
            CollectionAssert.AreEqual(new[] {1, 0, 0, 0}, impacts.GetImpactOffsets(1));
            CollectionAssert.AreEqual(new[] {0, 0, 0, 0}, impacts.GetImpactOffsets(2));
        }

        [Test]
        public void BackProcessingHitsEndOfArray()
        {
            var impacts = new ImpactOverview("TCA");
            impacts.ProcessNext();
            impacts.ProcessNext();

            impacts.ProcessNext();

            Assert.AreEqual(3, impacts.NextIndexToProcess);
            Assert.IsTrue(impacts.DoneProcessing);
            CollectionAssert.AreEqual(new[] {2, 1, 1, 0}, impacts.GetImpactOffsets(0));
            CollectionAssert.AreEqual(new[] {1, 0, 0, 0}, impacts.GetImpactOffsets(1));
            CollectionAssert.AreEqual(new[] {0, 0, 0, 0}, impacts.GetImpactOffsets(2));
        }
    }
}