﻿// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/genomic_range_query/
namespace GenomicRangeQuery
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int[] solution(string S, int[] P, int[] Q)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var impacts = new ImpactOverview(S);

            while (!impacts.DoneProcessing)
            {
                impacts.ProcessNext();
            }

            var result = new int[P.Length];
            for (int i = 0; i < result.Length; i++)
            {
                var maxOffset = Q[i] - P[i];
                var impactOffsets = impacts.GetImpactOffsets(P[i]);
                for (int impact = 4; impact > 0; impact--)
                {
                    if (impactOffsets[impact - 1] > maxOffset)
                    {
                        break;
                    }
                    result[i] = impact;
                }

            }

            return result;
        }
    }
}