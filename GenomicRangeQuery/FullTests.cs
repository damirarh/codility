﻿using NUnit.Framework;

namespace GenomicRangeQuery
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test("CAGCCTA", new []{2, 5, 0}, new []{4, 5, 6}, new []{2, 4, 1});
        }

        [Test]
        public void LastNucleotideNotA()
        {
            Test("CAGCCTC", new []{2, 5, 0}, new []{4, 5, 6}, new []{2, 4, 1});
        }

        private void Test(string S, int[] P, int[] Q, int[] expected)
        {
            var solution = new Solution();

            var actual = solution.solution(S, P, Q);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
