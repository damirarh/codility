﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/count_div/
namespace CountDiv
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int A, int B, int K)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var minimumMultiplier = (int)Math.Ceiling((double) A/K);
            var maximumMultiplier = (int)Math.Floor((double) B/K);
            return maximumMultiplier - minimumMultiplier + 1;
        }
    }
}