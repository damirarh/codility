﻿using NUnit.Framework;

namespace CountDiv
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(6, 11, 2, 3);
        }

        [Test]
        public void SameNumberNotDivisible()
        {
            Test(5, 5, 2, 0);
        }

        [Test]
        public void SameNumberDivisible()
        {
            Test(6, 6, 2, 1);
        }

        [Test]
        public void NoBoundsDivisible()
        {
            Test(5, 13, 3, 3);
        }

        [Test]
        public void LowerBoundDivisible()
        {
            Test(6, 13, 3, 3);
        }

        [Test]
        public void UpperBoundDivisible()
        {
            Test(5, 12, 3, 3);
        }

        [Test]
        public void BothBoundsDivisible()
        {
            Test(6, 12, 3, 3);
        }

        private void Test(int A, int B, int K, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(A, B, K);

            Assert.AreEqual(expected, actual);
        }
    }
}
