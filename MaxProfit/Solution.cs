﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/max_profit/
namespace MaxProfit
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var finder = new MaxProfitFinder();
            foreach (var price in A)
            {
                finder.Process(price);
            }
            return finder.MaxProfitSoFar;
        }
    }
}