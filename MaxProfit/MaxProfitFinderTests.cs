﻿using System;
using NUnit.Framework;

namespace MaxProfit
{
    [TestFixture]
    public class MaxProfitFinderTests
    {
        [Test]
        public void Initialize()
        {
            var finder = new MaxProfitFinder();

            Assert.AreEqual(Int32.MaxValue, finder.MinimumPriceSoFar);
            Assert.AreEqual(0, finder.MaxProfitSoldToday);
            Assert.AreEqual(0, finder.MaxProfitSoFar);
        }

        [Test]
        public void SampleStep1()
        {
            var finder = new MaxProfitFinder();

            finder.Process(23171);

            Assert.AreEqual(23171, finder.MinimumPriceSoFar);
            Assert.AreEqual(0, finder.MaxProfitSoldToday);
            Assert.AreEqual(0, finder.MaxProfitSoFar);
        }

        [Test]
        public void SampleStep2()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);

            finder.Process(21011);

            Assert.AreEqual(21011, finder.MinimumPriceSoFar);
            Assert.AreEqual(0, finder.MaxProfitSoldToday);
            Assert.AreEqual(0, finder.MaxProfitSoFar);
        }

        [Test]
        public void SampleStep3()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);
            finder.Process(21011);

            finder.Process(21123);

            Assert.AreEqual(21011, finder.MinimumPriceSoFar);
            Assert.AreEqual(112, finder.MaxProfitSoldToday);
            Assert.AreEqual(112, finder.MaxProfitSoFar);
        }

        [Test]
        public void SampleStep4()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);
            finder.Process(21011);
            finder.Process(21123);

            finder.Process(21366);

            Assert.AreEqual(21011, finder.MinimumPriceSoFar);
            Assert.AreEqual(355, finder.MaxProfitSoldToday);
            Assert.AreEqual(355, finder.MaxProfitSoFar);
        }

        [Test]
        public void SampleStep5()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);
            finder.Process(21011);
            finder.Process(21123);
            finder.Process(21366);

            finder.Process(21013);

            Assert.AreEqual(21011, finder.MinimumPriceSoFar);
            Assert.AreEqual(2, finder.MaxProfitSoldToday);
            Assert.AreEqual(355, finder.MaxProfitSoFar);
        }

        [Test]
        public void SampleStep6()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);
            finder.Process(21011);
            finder.Process(21123);
            finder.Process(21366);
            finder.Process(21013);

            finder.Process(21367);

            Assert.AreEqual(21011, finder.MinimumPriceSoFar);
            Assert.AreEqual(356, finder.MaxProfitSoldToday);
            Assert.AreEqual(356, finder.MaxProfitSoFar);
        }

        [Test]
        public void ExtraStep2()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);
            finder.Process(21011);
            finder.Process(21123);
            finder.Process(21366);
            finder.Process(21013);
            finder.Process(21367);
            finder.Process(21000);

            finder.Process(21360);

            Assert.AreEqual(21000, finder.MinimumPriceSoFar);
            Assert.AreEqual(360, finder.MaxProfitSoldToday);
            Assert.AreEqual(360, finder.MaxProfitSoFar);
        }

        [Test]
        public void ExtraStep1()
        {
            var finder = new MaxProfitFinder();
            finder.Process(23171);
            finder.Process(21011);
            finder.Process(21123);
            finder.Process(21366);
            finder.Process(21013);
            finder.Process(21367);

            finder.Process(21000);

            Assert.AreEqual(21000, finder.MinimumPriceSoFar);
            Assert.AreEqual(0, finder.MaxProfitSoldToday);
            Assert.AreEqual(356, finder.MaxProfitSoFar);
        }
    }
}