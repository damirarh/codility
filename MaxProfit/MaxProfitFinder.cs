﻿using System;

namespace MaxProfit
{
    public class MaxProfitFinder
    {
        public int MinimumPriceSoFar { get; private set; } = Int32.MaxValue;
        public int MaxProfitSoldToday { get; private set; }
        public int MaxProfitSoFar { get; private set; }

        public void Process(int price)
        {
            if (price < MinimumPriceSoFar)
            {
                MinimumPriceSoFar = price;
            }

            MaxProfitSoldToday = price - MinimumPriceSoFar;
            if (MaxProfitSoldToday > MaxProfitSoFar)
            {
                MaxProfitSoFar = MaxProfitSoldToday;
            }
        }
    }
}