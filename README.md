# Codility solutions

The repository contains solutions to tasks in [Codility](https://codility.com/programmers/) lessons, as coded in my [Livecoding.tv](https://www.livecoding.tv/damirarh/) stream.

At the top of the `Solution.cs` file in each project is a URL to the task on the site.

## License

The source code is licensed under the terms of the MIT License.