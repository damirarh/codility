﻿using NUnit.Framework;

namespace Fish
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new int[] {4, 3, 2, 1, 5}, new int[] {0, 1, 0, 0, 0}, 2);
        }

        private void Test(int[] input1, int[] input2, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input1, input2);

            Assert.AreEqual(expected, actual);
        }
    }
}
