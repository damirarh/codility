﻿using System;
using System.Collections.Generic;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/fish/
namespace Fish
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A, int[] B)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var upstreamFish = new Stack<int>();
            var survivingFish = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (B[i] == 1)
                {
                    upstreamFish.Push(A[i]);
                }
                else
                {
                    while ((upstreamFish.Count > 0) && (upstreamFish.Peek() < A[i]))
                    {
                        upstreamFish.Pop();
                    }
                    if (upstreamFish.Count == 0)
                    {
                        survivingFish++;
                    }
                }
            }
            return survivingFish + upstreamFish.Count;
        }
    }
}