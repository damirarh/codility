﻿using NUnit.Framework;

namespace NumberOfDiscIntersections
{
    [TestFixture]
    public class IntersectionCounterTests
    {
        [Test]
        public void Initialization()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });
            
            CollectionAssert.AreEqual(new [] {-4, -1, 0, 0, 2, 5}, counter.GetSortedLowerLimits());
            CollectionAssert.AreEqual(new [] {1, 4, 4, 5, 6, 8}, counter.GetSortedUpperLimits());
            Assert.AreEqual(0, counter.NextUpperLimitIndex);
            Assert.AreEqual(0, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(0, counter.IntersectionCount);
            Assert.IsTrue(counter.CanEvaluateNextLimit);
        }

        [Test]
        public void AfterStep1()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });

            counter.EvaluateNextLimit();
            
            Assert.AreEqual(1, counter.NextUpperLimitIndex);
            Assert.AreEqual(3, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(3, counter.IntersectionCount);
            Assert.IsTrue(counter.CanEvaluateNextLimit);
        }

        [Test]
        public void AfterStep2()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });
            counter.EvaluateNextLimit();

            counter.EvaluateNextLimit();
            
            Assert.AreEqual(2, counter.NextUpperLimitIndex);
            Assert.AreEqual(4, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(6, counter.IntersectionCount);
            Assert.IsTrue(counter.CanEvaluateNextLimit);
        }

        [Test]
        public void AfterStep3()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();

            counter.EvaluateNextLimit();
            
            Assert.AreEqual(3, counter.NextUpperLimitIndex);
            Assert.AreEqual(4, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(8, counter.IntersectionCount);
            Assert.IsTrue(counter.CanEvaluateNextLimit);
        }

        [Test]
        public void AfterStep4()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();

            counter.EvaluateNextLimit();
            
            Assert.AreEqual(4, counter.NextUpperLimitIndex);
            Assert.AreEqual(5, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(10, counter.IntersectionCount);
            Assert.IsTrue(counter.CanEvaluateNextLimit);
        }

        [Test]
        public void AfterStep5()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();

            counter.EvaluateNextLimit();
            
            Assert.AreEqual(5, counter.NextUpperLimitIndex);
            Assert.AreEqual(5, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(11, counter.IntersectionCount);
            Assert.IsTrue(counter.CanEvaluateNextLimit);
        }

        [Test]
        public void AfterStep6()
        {
            var counter = new IntersectionCounter(new[] { 1, 5, 2, 1, 4, 0 });
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();
            counter.EvaluateNextLimit();

            counter.EvaluateNextLimit();
            
            Assert.AreEqual(6, counter.NextUpperLimitIndex);
            Assert.AreEqual(5, counter.CurrentLowerLimitIndex);
            Assert.AreEqual(11, counter.IntersectionCount);
            Assert.IsFalse(counter.CanEvaluateNextLimit);
        }
    }
}