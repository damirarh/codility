﻿using System;
using System.Linq;

namespace NumberOfDiscIntersections
{
    public class IntersectionCounter
    {
        private readonly int[] _radiuses;
        private readonly long[] _lowerLimits;
        private readonly long[] _upperLimits;

        public IntersectionCounter(int[] radiuses)
        {
            _radiuses = radiuses;
            _lowerLimits = new long[radiuses.Length];
            _upperLimits = new long[radiuses.Length];

            for (long i = 0; i < radiuses.Length; i++)
            {
                _lowerLimits[i] = i - radiuses[i];
                _upperLimits[i] = i + radiuses[i];
            }

            Array.Sort(_lowerLimits);
            Array.Sort(_upperLimits);
        }

        public void EvaluateNextLimit()
        {
            var upperLimit = _upperLimits[NextUpperLimitIndex];
            while ((CurrentLowerLimitIndex + 1 < _radiuses.Length) && (_lowerLimits[CurrentLowerLimitIndex + 1] <= upperLimit))
            {
                CurrentLowerLimitIndex++;
            }
            IntersectionCount += CurrentLowerLimitIndex - NextUpperLimitIndex;
            NextUpperLimitIndex++;
        }

        public long[] GetSortedLowerLimits()
        {
            return _lowerLimits.ToArray();
        }

        public long[] GetSortedUpperLimits()
        {
            return _upperLimits.ToArray();
        }

        public int NextUpperLimitIndex { get; private set; }
        public int CurrentLowerLimitIndex { get; private set; }
        public int IntersectionCount { get; private set; }
        public bool CanEvaluateNextLimit => NextUpperLimitIndex < _radiuses.Length;
    }
}