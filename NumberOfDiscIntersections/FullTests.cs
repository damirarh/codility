﻿using NUnit.Framework;

namespace NumberOfDiscIntersections
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {1, 5, 2, 1, 4, 0}, 11);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
