﻿using System.Linq;
using NUnit.Framework;

namespace PermMissingElem
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {2, 3, 1, 5}, 4);
        }

        [Test]
        public void ZeroLength()
        {
            Test(new int[] {}, 1); // ?
        }

        [Test]
        public void LongArray()
        {
            var items = Enumerable.Range(1, 25)
                .Concat(Enumerable.Range(27, 20))
                .ToArray();
            Test(items, 26);
        }

        [Test]
        public void VeryLongArray()
        {
            var items = Enumerable.Range(1, 25)
                .Concat(Enumerable.Range(27, 100000))
                .ToArray();
            Test(items, 26);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
