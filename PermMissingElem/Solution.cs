﻿using System.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/perm_missing_elem
namespace PermMissingElem
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // sum = (n + 1) * (n + 2) / 2
            var expectedSum = ((long)A.Length + 1)*(A.Length + 2)/2;
            long actualSum = 0;
            for (int i = 0; i < A.Length; i++)
            {
                actualSum += A[i];
            }
            return (int)(expectedSum - actualSum);
        }
    }
}