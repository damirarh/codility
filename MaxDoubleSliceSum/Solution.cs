﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/max_double_slice_sum/
namespace MaxDoubleSliceSum
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var finder = new MaxSliceFinder();
            for (int i = 1; i < A.Length - 1; i++)
            {
                finder.Process(A[i]);
            }
            var maxSums = finder.GetMaxSums();

            var reverseFinder = new MaxSliceFinder();
            for (int i = A.Length - 2; i > 0; i--)
            {
                reverseFinder.Process(A[i]);
            }
            var maxSumsReverse = reverseFinder.GetMaxSums();

            var maxSlice = 0;
            for (int i = 0; i < maxSums.Length - 2; i++)
            {
                var slice = maxSums[i] + maxSumsReverse[maxSums.Length - 3 - i];
                if (slice > maxSlice)
                {
                    maxSlice = slice;
                }
            }
            if (maxSums.Length >= 2 && maxSums[maxSums.Length - 2] > maxSlice)
            {
                maxSlice = maxSums[maxSums.Length - 2];
            }

            if (maxSumsReverse.Length >= 2 && maxSumsReverse[maxSumsReverse.Length - 2] > maxSlice)
            {
                maxSlice = maxSumsReverse[maxSumsReverse.Length - 2];
            }

            return maxSlice;
        }
    }
}