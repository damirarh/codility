﻿using NUnit.Framework;

namespace MaxDoubleSliceSum
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {3, 2, 6, -1, 4, 5, -1, 2}, 17);
        }

        [Test]
        public void MinimumLength()
        {
            Test(new [] {3, 2, 6}, 0);
        }

        [Test]
        public void SingleItemOnly()
        {
            Test(new [] {3, 2, 1, 6}, 2);
        }

        [Test]
        public void NegativeOnly()
        {
            Test(new [] {3, -2, -1, 6}, 0);
        }

        [Test]
        public void Sample2()
        {
            Test(new[] {2, 1, 1, 0, 10, -100, 10, 0, 5}, 22);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
