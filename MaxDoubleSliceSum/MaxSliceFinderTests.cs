﻿using NUnit.Framework;

namespace MaxDoubleSliceSum
{
    [TestFixture]
    public class MaxSliceFinderTests
    {
        [Test]
        public void MaxSliceSums()
        {
            var finder = new MaxSliceFinder();

            finder.Process(2);
            finder.Process(6);
            finder.Process(-1);
            finder.Process(4);
            finder.Process(5);
            finder.Process(-1);
            finder.Process(-20);
            finder.Process(-17);

            CollectionAssert.AreEqual(new [] {2, 8, 7, 11, 16, 15, 0, 0}, finder.GetMaxSums());
        }
    }
}