﻿using System.Collections.Generic;
using NUnit.Framework;

namespace MaxDoubleSliceSum
{
    public class MaxSliceFinder
    {
        private readonly List<int> _maxSumEndingHere = new List<int>();

        public void Process(int item)
        {
            var previousSum = _maxSumEndingHere.Count > 0 ? _maxSumEndingHere[_maxSumEndingHere.Count - 1] : 0;
            var maxSumEndingHere = previousSum + item;
            if (maxSumEndingHere < 0)
            {
                maxSumEndingHere = 0;
            }
            _maxSumEndingHere.Add(maxSumEndingHere);
        }

        public int[] GetMaxSums()
        {
            return _maxSumEndingHere.ToArray();
        }
    }
}