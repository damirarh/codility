﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/dominator/
namespace Dominator
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var stack = new DominatorCandidateStack();
            foreach (var item in A)
            {
                stack.Push(item);
            }
            if (!stack.Value.HasValue)
            {
                return -1;
            }
            var count = 0;
            var index = 0;
            for (var i = 0; i < A.Length; i++)
            {
                if (A[i] == stack.Value.Value)
                {
                    index = i;
                    count++;
                }
            }
            if (count > A.Length / 2)
            {
                return index;
            }
            return -1;
        }
    }
}