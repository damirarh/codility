﻿using NUnit.Framework;

namespace Dominator
{
    [TestFixture]
    public class DominatorCandidateStackTests
    {
        [Test]
        public void Initialize()
        {
            var stack = new DominatorCandidateStack();

            Assert.IsNull(stack.Value);
            Assert.AreEqual(0, stack.CurrentCount);
        }

        [Test]
        public void PushIntoEmpty()
        {
            var stack = new DominatorCandidateStack();

            stack.Push(1);

            Assert.AreEqual(1, stack.Value);
            Assert.AreEqual(1, stack.CurrentCount);
        }

        [Test]
        public void PushSameValue()
        {
            var stack = new DominatorCandidateStack();
            stack.Push(1);

            stack.Push(1);

            Assert.AreEqual(1, stack.Value);
            Assert.AreEqual(2, stack.CurrentCount);
        }

        [Test]
        public void PushDifferentValueNonEmpty()
        {
            var stack = new DominatorCandidateStack();
            stack.Push(1);
            stack.Push(1);

            stack.Push(2);

            Assert.AreEqual(1, stack.Value);
            Assert.AreEqual(1, stack.CurrentCount);
        }

        [Test]
        public void PushDifferentValueEmpty()
        {
            var stack = new DominatorCandidateStack();
            stack.Push(1);

            stack.Push(2);

            Assert.IsNull(stack.Value);
            Assert.AreEqual(0, stack.CurrentCount);
        }
    }
}