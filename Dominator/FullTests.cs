﻿using NUnit.Framework;

namespace Dominator
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {3, 4, 3, 2, 3, -1, 3, 3}, 7);
        }

        [Test]
        public void EndsWithEmptyStack()
        {
            Test(new [] {3, 4, 3, 2, 3, -1, 3, 2}, -1);
        }

        [Test]
        public void NonEmptyStackNoDominator()
        {
            Test(new [] {3, 4, 3, 2, 3, -1, 2, 2}, -1);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
