﻿namespace PassingCars
{
    public enum CarDirection
    {
        Eastbound = 0,
        Westbound = 1
    }
}