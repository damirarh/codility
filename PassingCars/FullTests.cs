﻿using System.Linq;
using NUnit.Framework;

namespace PassingCars
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] { 0, 1, 0, 1, 1 }, 5);
        }

        [Test]
        public void SingleCarEastbound()
        {
            Test(new [] { 0 }, 0);
        }

        [Test]
        public void SingleCarWestbound()
        {
            Test(new [] { 1 }, 0);
        }

        [Test]
        public void AllCarsEastbound()
        {
            Test(new [] { 0, 0, 0, 0 }, 0);
        }

        [Test]
        public void AllCarsWestbound()
        {
            Test(new [] { 1, 1, 1, 1 }, 0);
        }

        [Test]
        public void EastboundFollowedByWestbound()
        {
            Test(new [] { 0, 0, 0, 1, 1, 1 }, 9);
        }

        [Test]
        public void WestboundFollowedByEastbound()
        {
            Test(new [] { 1, 1, 1, 0, 0, 0 }, 0);
        }

        [Test]
        public void MoreThan1BillionPassings()
        {
            var eastbound = Enumerable.Repeat(0, 50000);
            var westbound = Enumerable.Repeat(1, 50000);
            var input = eastbound.Concat(westbound).ToArray();
            Test(input, -1);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
