﻿using System;

namespace PassingCars
{
    public class CarCounter
    {
        private readonly CarDirection[] _cars;

        public CarCounter(CarDirection[] cars)
        {
            _cars = cars;
        }

        public int PassedEastboundCars { get; private set; }
        public int PassingPairs { get; private set; }
        public int NextCarSlot { get; private set; }

        public bool CanMoveToNextCarSlot => NextCarSlot < _cars.Length;

        public void MoveToNextCar()
        {
            if (!CanMoveToNextCarSlot)
            {
                throw new InvalidOperationException("No more cars.");
            }

            switch (_cars[NextCarSlot])
            {
                case CarDirection.Eastbound:
                    PassedEastboundCars++;
                    break;
                case CarDirection.Westbound:
                    PassingPairs += PassedEastboundCars;
                    break;
                default:
                    throw new InvalidOperationException("Unrecognized car direction.");
            }

            NextCarSlot++;
        }
    }
}