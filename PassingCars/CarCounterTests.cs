﻿using NUnit.Framework;

namespace PassingCars
{
    [TestFixture]
    public class CarCounterTests
    {
        [Test]
        public void Initialization()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Eastbound,
                CarDirection.Westbound,
                CarDirection.Westbound,
                CarDirection.Eastbound
            });

            Assert.AreEqual(0, counter.NextCarSlot);
            Assert.AreEqual(0, counter.PassedEastboundCars);
            Assert.AreEqual(0, counter.PassingPairs);
            Assert.IsTrue(counter.CanMoveToNextCarSlot);
        }

        [Test]
        public void PassingWestboundCarWithNoEastboundBefore()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Westbound,
                CarDirection.Westbound
            });

            counter.MoveToNextCar();

            Assert.AreEqual(1, counter.NextCarSlot);
            Assert.AreEqual(0, counter.PassedEastboundCars);
            Assert.AreEqual(0, counter.PassingPairs);
            Assert.IsTrue(counter.CanMoveToNextCarSlot);
        }

        [Test]
        public void PassingWestboundCarWith1EastboundBefore()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Eastbound,
                CarDirection.Westbound,
                CarDirection.Westbound
            });
            counter.MoveToNextCar();

            counter.MoveToNextCar();

            Assert.AreEqual(2, counter.NextCarSlot);
            Assert.AreEqual(1, counter.PassedEastboundCars);
            Assert.AreEqual(1, counter.PassingPairs);
            Assert.IsTrue(counter.CanMoveToNextCarSlot);
        }

        [Test]
        public void PassingWestboundCarWith2EastboundBefore()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Eastbound,
                CarDirection.Eastbound,
                CarDirection.Westbound,
                CarDirection.Westbound
            });
            counter.MoveToNextCar();
            counter.MoveToNextCar();

            counter.MoveToNextCar();

            Assert.AreEqual(3, counter.NextCarSlot);
            Assert.AreEqual(2, counter.PassedEastboundCars);
            Assert.AreEqual(2, counter.PassingPairs);
            Assert.IsTrue(counter.CanMoveToNextCarSlot);
        }

        [Test]
        public void PassedAllCars()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Eastbound
            });
            counter.MoveToNextCar();

            Assert.AreEqual(1, counter.NextCarSlot);
            Assert.AreEqual(1, counter.PassedEastboundCars);
            Assert.AreEqual(0, counter.PassingPairs);
            Assert.IsFalse(counter.CanMoveToNextCarSlot);
        }

        [Test]
        public void PassingNextEastboundCar()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Eastbound,
                CarDirection.Eastbound,
                CarDirection.Westbound,
                CarDirection.Westbound,
            });
            counter.MoveToNextCar();

            counter.MoveToNextCar();

            Assert.AreEqual(2, counter.NextCarSlot);
            Assert.AreEqual(2, counter.PassedEastboundCars);
            Assert.AreEqual(0, counter.PassingPairs);
            Assert.IsTrue(counter.CanMoveToNextCarSlot);
        }

        [Test]
        public void PassingFirstEastboundCar()
        {
            var counter = new CarCounter(new CarDirection[]
            {
                CarDirection.Eastbound,
                CarDirection.Westbound,
                CarDirection.Westbound,
                CarDirection.Eastbound
            });

            counter.MoveToNextCar();

            Assert.AreEqual(1, counter.NextCarSlot);
            Assert.AreEqual(1, counter.PassedEastboundCars);
            Assert.AreEqual(0, counter.PassingPairs);
            Assert.IsTrue(counter.CanMoveToNextCarSlot);
        }
    }
}