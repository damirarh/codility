﻿using System;
using System.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/passing_cars/
namespace PassingCars
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var convertedInput = A.Select(car => (CarDirection) car).ToArray();
            var counter = new CarCounter(convertedInput);
            while (counter.CanMoveToNextCarSlot)
            {
                counter.MoveToNextCar();
                if (counter.PassingPairs > 1000000000)
                {
                    return -1;
                }
            }
            return counter.PassingPairs;
        }
    }
}