﻿using System;
using NUnit.Framework.Constraints;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/equi_leader/
namespace EquiLeader
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var stack = new DominatorCandidateStack();
            foreach (var item in A)
            {
                stack.Push(item);
            }

            if (!stack.Value.HasValue)
            {
                return 0;
            }

            var totalCount = 0;
            foreach (var item in A)
            {
                if (item == stack.Value.Value)
                {
                    totalCount++;
                }
            }

            var leftCount = 0;
            var equiLeadersCount = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] == stack.Value.Value)
                {
                    leftCount++;
                }
                var rightCount = totalCount - leftCount;
                if ((leftCount > (i + 1) / 2) && (rightCount > (A.Length - i - 1) / 2))
                {
                    equiLeadersCount++;
                }
            }

            return equiLeadersCount;
        }
    }
}