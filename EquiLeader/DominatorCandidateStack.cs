﻿namespace EquiLeader
{
    public class DominatorCandidateStack
    {
        public int? Value { get; private set; }
        public int CurrentCount { get; private set; }

        public void Push(int item)
        {
            if (!Value.HasValue || (Value.Value == item))
            {
                Value = item;
                CurrentCount++;
            }
            else
            {
                CurrentCount--;
            }
            if (CurrentCount == 0)
            {
                Value = null;
            }
        }
    }
}