﻿using NUnit.Framework;

namespace EquiLeader
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new[] {4, 3, 4, 4, 4, 2}, 2);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
