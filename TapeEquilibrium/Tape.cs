﻿using System;
using System.Linq;

namespace TapeEquilibrium
{
    public class Tape
    {
        private readonly int[] _items;

        public Tape(int[] items)
        {
            _items = items;
            LeftSum = items[0];
            RightSum = items.Sum() - LeftSum;
            SplitIndex = 1;
        }

        public int LeftSum { get; private set; }

        public int RightSum { get; private set; }

        public int SplitIndex { get; private set; }

        public int Diff => Math.Abs(LeftSum - RightSum);

        public bool CanMoveSplitLeft => SplitIndex > 1;

        public bool CanMoveSplitRight => SplitIndex < _items.Length - 1;

        public void MoveSplitLeft()
        {
            if (!CanMoveSplitLeft)
            {
                throw new InvalidOperationException();
            }

            SplitIndex--;
            LeftSum -= _items[SplitIndex];
            RightSum += _items[SplitIndex];
        }

        public void MoveSplitRight()
        {
            if (!CanMoveSplitRight)
            {
                throw new InvalidOperationException();
            }

            LeftSum += _items[SplitIndex];
            RightSum -= _items[SplitIndex];
            SplitIndex++;
        }
    }
}