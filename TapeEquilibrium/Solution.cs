﻿// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/tape_equilibrium
namespace TapeEquilibrium
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            var tape = new Tape(A);
            var minDiff = tape.Diff;
            while (tape.CanMoveSplitRight)
            {
                tape.MoveSplitRight();
                minDiff = tape.Diff < minDiff ? tape.Diff : minDiff;
            }
            return minDiff;
        }
    }
}