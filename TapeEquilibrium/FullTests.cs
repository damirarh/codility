﻿using NUnit.Framework;

namespace TapeEquilibrium
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] { 3, 1, 2, 4, 3 }, 1);
        }

        [Test]
        public void TwoItems()
        {
            Test(new [] { -10, 10 }, 20);
        }

        [Test]
        public void NegativeItems()
        {
            Test(new [] { 3, -5, 2, -1, 4 }, 3);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
