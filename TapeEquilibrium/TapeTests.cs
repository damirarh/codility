﻿using System.Runtime.CompilerServices;
using NUnit.Framework;

namespace TapeEquilibrium
{
    [TestFixture]
    public class TapeTests
    {
        [Test]
        public void Initialization()
        {
            var items = new[] {3, 1, 2, 4, 3};

            var tape = new Tape(items);

            Assert.AreEqual(3, tape.LeftSum);
            Assert.AreEqual(10, tape.RightSum);
            Assert.AreEqual(7, tape.Diff);
            Assert.AreEqual(1, tape.SplitIndex);
            Assert.IsFalse(tape.CanMoveSplitLeft);
            Assert.IsTrue(tape.CanMoveSplitRight);
        }

        [Test]
        public void MoveRightWhenAllowed()
        {
            var items = new[] { 3, 1, 2, 4, 3 };

            var tape = new Tape(items);
            tape.MoveSplitRight();

            Assert.AreEqual(4, tape.LeftSum);
            Assert.AreEqual(9, tape.RightSum);
            Assert.AreEqual(5, tape.Diff);
            Assert.AreEqual(2, tape.SplitIndex);
            Assert.IsTrue(tape.CanMoveSplitLeft);
            Assert.IsTrue(tape.CanMoveSplitRight);
        }

        [Test]
        public void MoveLeftWhenAllowed()
        {
            var items = new[] { 3, 1, 2, 4, 3 };

            var tape = new Tape(items);
            tape.MoveSplitRight();
            tape.MoveSplitLeft();

            Assert.AreEqual(3, tape.LeftSum);
            Assert.AreEqual(10, tape.RightSum);
            Assert.AreEqual(7, tape.Diff);
            Assert.AreEqual(1, tape.SplitIndex);
            Assert.IsFalse(tape.CanMoveSplitLeft);
            Assert.IsTrue(tape.CanMoveSplitRight);
        }

        [Test]
        public void NoMoveAllowed()
        {
            var items = new[] { -10, 10 };

            var tape = new Tape(items);

            Assert.AreEqual(-10, tape.LeftSum);
            Assert.AreEqual(10, tape.RightSum);
            Assert.AreEqual(20, tape.Diff);
            Assert.AreEqual(1, tape.SplitIndex);
            Assert.IsFalse(tape.CanMoveSplitLeft);
            Assert.IsFalse(tape.CanMoveSplitRight);
        }
    }
}