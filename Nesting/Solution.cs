﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/nesting/
namespace Nesting
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(string S)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)

            int openBraceCount = 0;
            foreach (var character in S)
            {
                switch (character)
                {
                    case '(':
                        openBraceCount++;
                        break;
                    case ')':
                        openBraceCount--;
                        break;
                }
                if (openBraceCount < 0)
                {
                    return 0;
                }
            }
            if (openBraceCount != 0)
            {
                return 0;
            }

            return 1;
        }
    }
}