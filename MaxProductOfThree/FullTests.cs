﻿using System.Linq;
using NUnit.Framework;

namespace MaxProductOfThree
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {-3, 1, 2, -2, 5, 6}, 60);
        }

        [Test]
        public void ResultWith2Negatives()
        {
            Test(new [] {-3, 1, 2, -2, -5, 6}, 90);
        }

        [Test]
        public void LatePositive()
        {
            Test(new [] {-3, 1, 2, -2, -5, -6}, 60);
        }

        [Test]
        public void IncludingZeros()
        {
            Test(new [] {-3, 0, -2, -2, -5, -6}, 0);
        }

        [Test]
        public void NegativeOnly()
        {
            Test(new [] {-3, -1, -2, -2, -5, -6}, -4);
        }

        [Test]
        public void MaxNegativeLargest()
        {
            Test(new [] { -1000, 500, 10, -10, 10 }, 5000000);
        }

        [Test]
        public void WrongResult()
        {
            Test(new [] { 4, 7, 3, 2, 1, -3, -5 }, 105);
        }

        [Test]
        public void Large()
        {
            Test(Enumerable.Repeat(-2, 100000).ToArray(), -8);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
