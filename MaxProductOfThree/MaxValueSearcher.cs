﻿using System;
using System.Linq;

namespace MaxProductOfThree
{
    public class MaxValueSearcher
    {
        private readonly int[] _array;
        private readonly int?[] _maximums = new int?[3];
        private readonly int?[] _minimums = new int?[2];

        public MaxValueSearcher(int[] array)
        {
            _array = array;
        }

        public void EvaluateNextCandidate()
        {
            if (CandidatesEvaluated >= _array.Length)
            {
                throw new InvalidOperationException("All candidates already evaluated");
            }
            var next = _array[CandidatesEvaluated];
            for (int i = 0; i < _maximums.Length; i++)
            {
                if (!_maximums[i].HasValue)
                {
                    _maximums[i] = next;
                    break;
                }
                if (_maximums[i] <= next)
                {
                    for (int j = _maximums.Length - 1; j > i; j--)
                    {
                        _maximums[j] = _maximums[j - 1];
                    }
                    _maximums[i] = next;
                    break;
                }
            }
            for (int i = 0; i < _minimums.Length; i++)
            {
                if (!_minimums[i].HasValue)
                {
                    _minimums[i] = next;
                    break;
                }
                if (_minimums[i] >= next)
                {
                    for (int j = _minimums.Length - 1; j > i; j--)
                    {
                        _minimums[j] = _minimums[j - 1];
                    }
                    _minimums[i] = next;
                    break;
                }
            }
            CandidatesEvaluated++;
        }

        public int[] GetMaximums()
        {
            return _maximums.Where(x => x.HasValue).Select(x => x.Value).ToArray();
        }

        public int[] GetMinimums()
        {
            return _minimums.Where(x => x.HasValue).Select(x => x.Value).ToArray();
        }

        public int CandidatesEvaluated { get; private set; }
        public bool AllCandidatesEvaluated => CandidatesEvaluated == _array.Length;
    }
}