﻿using System;
using NUnit.Framework;

namespace MaxProductOfThree
{
    [TestFixture]
    public class MaxValueSearcherTests
    {
        [Test]
        public void Initialization()
        {
            var searcher = new MaxValueSearcher(new int[] { });

            Assert.AreEqual(0, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new int[0], searcher.GetMaximums());
            CollectionAssert.AreEqual(new int[0], searcher.GetMinimums());
        }

        [Test]
        public void PositivePositivePositive()
        {
            var searcher = new MaxValueSearcher(new int[] { 1, 2, 3 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new [] { 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new [] { 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1, 2 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 3, 2, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1, 2 }, searcher.GetMinimums());

            Assert.Throws<InvalidOperationException>(() => searcher.EvaluateNextCandidate());
        }

        [Test]
        public void NegativePositivePositivePositive()
        {
            var searcher = new MaxValueSearcher(new int[] { 1, 2, -4, 3 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1, 2 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1, -4 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -4, 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(4, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 3, 2, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -4, 1 }, searcher.GetMinimums());

            Assert.Throws<InvalidOperationException>(() => searcher.EvaluateNextCandidate());
        }

        [Test]
        public void PositiveNegativePositivePositive()
        {
            var searcher = new MaxValueSearcher(new int[] { 1, 2, -4, 5 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1, 2 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1, -4 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -4, 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(4, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 5, 2, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -4, 1 }, searcher.GetMinimums());

            Assert.Throws<InvalidOperationException>(() => searcher.EvaluateNextCandidate());
        }

        [Test]
        public void PositivePositiveNegativePositive()
        {
            var searcher = new MaxValueSearcher(new int[] { 1, 3, -2, 5 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 3, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1, 3 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 3, 1, -2 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -2, 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(4, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 5, 3, 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -2, 1 }, searcher.GetMinimums());

            Assert.Throws<InvalidOperationException>(() => searcher.EvaluateNextCandidate());
        }

        [Test]
        public void NegativeNegativeNegativePositive()
        {
            var searcher = new MaxValueSearcher(new int[] { 1, -2, -5, -4 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1, -2 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -2, 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1, -2, -5 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -5, -2 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(4, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1, -2, -4 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -5, -4 }, searcher.GetMinimums());

            Assert.Throws<InvalidOperationException>(() => searcher.EvaluateNextCandidate());
        }

        [Test]
        public void SampleData()
        {
            var searcher = new MaxValueSearcher(new[] { -3, 1, 2, -2, 5, 6 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { -3 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 1, -3 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3, 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1, -3 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3, 1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(4, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { 2, 1, -2 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3, -2 }, searcher.GetMinimums());
        }

        [Test]
        public void NegativeNegativeNegativeNegative()
        {
            var searcher = new MaxValueSearcher(new[] { -3, -1, -2, -2 });

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(1, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { -3 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(2, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { -1, -3 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3, -1 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(3, searcher.CandidatesEvaluated);
            Assert.IsFalse(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { -1, -2, -3 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3, -2 }, searcher.GetMinimums());

            searcher.EvaluateNextCandidate();
            Assert.AreEqual(4, searcher.CandidatesEvaluated);
            Assert.IsTrue(searcher.AllCandidatesEvaluated);
            CollectionAssert.AreEqual(new[] { -1, -2, -2 }, searcher.GetMaximums());
            CollectionAssert.AreEqual(new[] { -3, -2 }, searcher.GetMinimums());

            Assert.Throws<InvalidOperationException>(() => searcher.EvaluateNextCandidate());
        }
    }
}