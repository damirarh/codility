﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/max_product_of_three/
namespace MaxProductOfThree
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var searcher = new MaxValueSearcher(A);
            while (!searcher.AllCandidatesEvaluated)
            {
                searcher.EvaluateNextCandidate();
            }
            var maximums = searcher.GetMaximums();
            var minimums = searcher.GetMinimums();

            return Math.Max(
                maximums[0] * maximums[1] * maximums[2],
                maximums[0] * minimums[0] * minimums[1]
                );
        }
    }
}