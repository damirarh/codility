﻿using NUnit.Framework;

namespace MinAvgTwoSlice
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {4, 2, 2, 5, 1, 5, 8}, 1);
        }

        [Test]
        public void Length2()
        {
            Test(new [] { 4, 2 }, 0);
        }

        [Test]
        public void Length3()
        {
            Test(new [] { 4, 2, 1 }, 1);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
