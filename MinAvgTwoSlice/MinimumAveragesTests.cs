﻿using System;
using NUnit.Framework;

namespace MinAvgTwoSlice
{
    [TestFixture]
    public class MinimumAveragesTests
    {
        [Test]
        public void Initialization()
        {
            var averages = new MinimumAverages(new int[] { 1, 1 });

            Assert.AreEqual(0, averages.NextIndexToCalculate);
            Assert.AreEqual(0, averages.RunningSum2);
            Assert.AreEqual(0, averages.RunningSum3);
            Assert.IsFalse(averages.CalculationDone);
        }

        [Test]
        public void FirstIndexMinimumAverage2()
        {
            var averages = new MinimumAverages(new int[] { 1, 1, 4 });

            averages.CalculateNext();

            Assert.AreEqual(1, averages.NextIndexToCalculate);
            Assert.AreEqual(2, averages.RunningSum2);
            Assert.AreEqual(6, averages.RunningSum3);
            Assert.IsFalse(averages.CalculationDone);
            Assert.AreEqual(1, averages[0]);
        }

        [Test]
        public void FirstIndexMinimumAverage3()
        {
            var averages = new MinimumAverages(new int[] { 7, 1, 1 });

            averages.CalculateNext();

            Assert.AreEqual(1, averages.NextIndexToCalculate);
            Assert.AreEqual(8, averages.RunningSum2);
            Assert.AreEqual(9, averages.RunningSum3);
            Assert.IsFalse(averages.CalculationDone);
            Assert.AreEqual(3, averages[0]);
        }

        [Test]
        public void NextIndex()
        {
            var averages = new MinimumAverages(new int[] { 7, 1, 1, 1 });
            averages.CalculateNext();

            averages.CalculateNext();

            Assert.AreEqual(2, averages.NextIndexToCalculate);
            Assert.AreEqual(2, averages.RunningSum2);
            Assert.AreEqual(3, averages.RunningSum3);
            Assert.IsFalse(averages.CalculationDone);
            Assert.AreEqual(1, averages[1]);
        }

        [Test]
        public void Average3OutOfBounds()
        {
            var averages = new MinimumAverages(new int[] { 7, 1, 1 });
            averages.CalculateNext();

            averages.CalculateNext();

            Assert.AreEqual(2, averages.NextIndexToCalculate);
            Assert.AreEqual(2, averages.RunningSum2);
            Assert.AreEqual(Int32.MaxValue, averages.RunningSum3);
            Assert.IsTrue(averages.CalculationDone);
            Assert.AreEqual(1, averages[1]);
        }
    }
}