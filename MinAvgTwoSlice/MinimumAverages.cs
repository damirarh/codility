﻿using System;

namespace MinAvgTwoSlice
{
    public class MinimumAverages
    {
        private readonly int[] _values;
        private readonly double[] _minimumAverages;

        public MinimumAverages(int[] values)
        {
            _values = values;
            _minimumAverages = new double[_values.Length - 1];
        }

        public int RunningSum2 { get; private set; }
        public int RunningSum3 { get; private set; }
        public int NextIndexToCalculate { get; private set; }
        public int Length => _minimumAverages.Length;
        public bool CalculationDone => NextIndexToCalculate >= _values.Length - 1;

        public double this[int index] => _minimumAverages[index];

        public void CalculateNext()
        {
            if (NextIndexToCalculate == 0)
            {
                // do full calculation at the beginning
                RunningSum2 = _values[0] + _values[1];
            }
            else
            {
                // incremental calculation later
                RunningSum2 = RunningSum3 - _values[NextIndexToCalculate - 1];
            }
            if (NextIndexToCalculate + 2 < _values.Length)
            {
                RunningSum3 = RunningSum2 + _values[NextIndexToCalculate + 2];
            }
            else
            {
                // out of bounds, no more values available, 
                // so average of 3 can't be smaller than average of 2
                RunningSum3 = Int32.MaxValue;
            }

            var average2 = (double) RunningSum2/2;
            var average3 = (double) RunningSum3/3;
            _minimumAverages[NextIndexToCalculate] = Math.Min(average2, average3);

            NextIndexToCalculate++;
        }
    }
}