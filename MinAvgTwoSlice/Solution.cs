﻿// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/min_avg_two_slice/
namespace MinAvgTwoSlice
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var averages = new MinimumAverages(A);

            while (!averages.CalculationDone)
            {
                averages.CalculateNext();
            }
            var minAverage = double.MaxValue;
            var minAverageIndex = 0;
            for (int i = 0; i < averages.Length; i++)
            {
                if (averages[i] < minAverage)
                {
                    minAverage = averages[i];
                    minAverageIndex = i; 
                }
            }
            return minAverageIndex;
        }
    }
}