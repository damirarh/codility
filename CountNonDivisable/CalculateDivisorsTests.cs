﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace CountNonDivisable
{
    [TestFixture]
    public class CalculateDivisorsTests
    {
        [Test]
        public void CalculateFor9()
        {
            var solution = new Solution();

            var actual = solution.CalculateDivisors(9, Enumerable.Range(1, 9));

            var expected = new[]
            {
                null,
                new List<int>(new [] { 1 }), 
                new List<int>(new [] { 1, 2 }), 
                new List<int>(new [] { 1, 3 }), 
                new List<int>(new [] { 1, 4, 2 }), 
                new List<int>(new [] { 1, 5 }), 
                new List<int>(new [] { 1, 6, 2, 3 }), 
                new List<int>(new [] { 1, 7 }), 
                new List<int>(new [] { 1, 8, 2, 4 }), 
                new List<int>(new [] { 1, 9, 3 })
            };

            CollectionAssert.AreEquivalent(expected, actual);
        }
    }
}