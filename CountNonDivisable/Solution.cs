﻿using System;
using System.Collections.Generic;
using System.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

namespace CountNonDivisable
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int[] solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var maxInput = A.Max();
            var occurrences = CountOccurrences(A);
            var divisors = CalculateDivisors(maxInput, occurrences.Keys);

            var nonDivisorCount = new int[A.Length];
            var lookup = new Dictionary<int, int>();
            for (int i = 0; i < A.Length; i++)
            {
                var sum = 0;
                if (!lookup.TryGetValue(A[i], out sum))
                {
                    foreach (var divisor in divisors[A[i]])
                    {
                        var count = 0;
                        occurrences.TryGetValue(divisor, out count);
                        sum += count;
                    }
                    lookup[A[i]] = sum;
                }
                nonDivisorCount[i] = A.Length - sum;
            }
            return nonDivisorCount;
        }

        public HashSet<int>[] CalculateDivisors(int maxNumber, IEnumerable<int> numbers)
        {
            var divisors = new HashSet<int>[maxNumber + 1];
            for (int divisor = 1; divisor <= Math.Ceiling(Math.Sqrt(maxNumber)); divisor++)
            {
                for (int multiple = divisor; multiple <= maxNumber; multiple+=divisor)
                {
                    // Assuming numbers will be a KeyCollection from dictionary with quick lookup
                    if (numbers.Contains(multiple))
                    {
                        if (divisors[multiple] == null)
                        {
                            divisors[multiple] = new HashSet<int>();
                        }
                        divisors[multiple].Add(divisor);
                        divisors[multiple].Add(multiple / divisor);
                    }
                }
            }
            return divisors;
        }

        public Dictionary<int, int> CountOccurrences(int[] numbers)
        {
            var occurrences = new Dictionary<int, int>();
            foreach (int number in numbers)
            {
                if (occurrences.ContainsKey(number))
                {
                    occurrences[number]++;
                }
                else
                {
                    occurrences[number] = 1;
                }
            }
            return occurrences;
        }
    }
}