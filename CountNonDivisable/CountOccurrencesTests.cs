﻿using System.Collections.Generic;
using NUnit.Framework;

namespace CountNonDivisable
{
    [TestFixture]
    public class CountOccurrencesTests
    {
        [Test]
        public void SampleData()
        {
            var solution = new Solution();

            var actual = solution.CountOccurrences(new[] {3, 1, 2, 3, 6});

            var expected = new Dictionary<int, int>
            {
                [1] = 1,
                [2] = 1,
                [3] = 2,
                [6] = 1
            };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}