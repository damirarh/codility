﻿using System.Linq;
using NUnit.Framework;

namespace CountNonDivisable
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new[] {3, 1, 2, 3, 6}, new []{2, 4, 3, 2, 0});
        }

        [Test]
        public void NonOccurringDivisors()
        {
            Test(new[] {3, 1, 3, 3, 6}, new []{1, 4, 1, 1, 0});
        }

        [Test]
        public void Range()
        {
            Test(Enumerable.Range(1, 20).ToArray(), new []{19, 18, 18, 17, 18, 16, 18, 16, 17, 16, 18, 14, 18, 16, 16, 15, 18, 14, 18, 14});
        }

        [Test]
        public void Large()
        {
            Test(Enumerable.Repeat(100000, 50000).ToArray(), Enumerable.Repeat(0, 50000).ToArray());
        }

        private void Test(int[] input, int[] expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
