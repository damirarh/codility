﻿// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/frog_river_one
namespace FrogRiverOne
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int X, int[] A)
        // ReSharper restore InconsistentNaming
        {
            var river = new River(X);

            for (var second = 0; second < A.Length; second++)
            {
                river.AddLeaf(A[second]);
                if (river.MissingLeaves == 0)
                {
                    return second;
                }
            }
            return -1;
        }
    }
}