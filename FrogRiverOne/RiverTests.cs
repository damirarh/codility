﻿using NUnit.Framework;

namespace FrogRiverOne
{
    [TestFixture]
    public class RiverTests
    {
        [Test]
        public void Initialization()
        {
            var river = new River(5);

            Assert.AreEqual(5, river.MissingLeaves);
        }

        [Test]
        public void AddFirstLeaf()
        {
            var river = new River(3);

            river.AddLeaf(2);

            Assert.AreEqual(2, river.MissingLeaves);
        }

        [Test]
        public void AddNewLeaf()
        {
            var river = new River(3);
            river.AddLeaf(2);

            river.AddLeaf(1);

            Assert.AreEqual(1, river.MissingLeaves);
        }

        [Test]
        public void AddSameLeaf()
        {
            var river = new River(3);
            river.AddLeaf(2);

            river.AddLeaf(2);

            Assert.AreEqual(2, river.MissingLeaves);
        }
    }
}