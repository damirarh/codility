﻿using NUnit.Framework;

namespace FrogRiverOne
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(5, new [] {1, 3, 1, 4, 2, 3, 5, 4}, 6);
        }

        [Test]
        public void MissingLeaf()
        {
            Test(4, new [] {1, 3, 1, 4, 1, 3, 3, 4}, -1);
        }

        [Test]
        public void SingleLeaf()
        {
            Test(2, new [] {2}, -1);
        }

        [Test]
        public void RiverWidth1()
        {
            Test(1, new [] {1, 1, 1}, 0);
        }

        private void Test(int X, int[] A, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(X, A);

            Assert.AreEqual(expected, actual);
        }
    }
}
