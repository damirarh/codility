﻿using System.Collections;

namespace FrogRiverOne
{
    public class River
    {
        private readonly BitArray _leafSlots;

        public River(int width)
        {
            MissingLeaves = width;
            _leafSlots = new BitArray(width);
        }

        public int MissingLeaves { get; private set; }

        public void AddLeaf(int slot)
        {
            var index = slot - 1; // index is 0 based, leaves are 1 based
            if (!_leafSlots[index])
            {
                MissingLeaves--;
            }
            _leafSlots[index] = true;
        }
    }
}