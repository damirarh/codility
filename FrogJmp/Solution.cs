﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/frog_jmp
namespace FrogJmp
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int X, int Y, int D)
        // ReSharper restore InconsistentNaming
        {
            // X + n * D >= Y
            return (int)Math.Ceiling(((double)Y - X) / D);
        }
    }
}