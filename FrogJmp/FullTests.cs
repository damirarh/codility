﻿using NUnit.Framework;

namespace FrogJmp
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(10, 85, 30, 3);
        }

        [Test]
        public void NoMove()
        {
            Test(10, 10, 50, 0);
        }

        [Test]
        public void ExactMove()
        {
            Test(10, 50, 20, 2);
        }

        [Test]
        public void OneBelow()
        {
            Test(10, 51, 20, 3);
        }

        [Test]
        public void OneOver()
        {
            Test(10, 49, 20, 2);
        }

        private void Test(int X, int Y, int D, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(X, Y, D);

            Assert.AreEqual(expected, actual);
        }
    }
}
