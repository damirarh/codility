﻿using System;
using System.Collections.Generic;
using System.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/brackets/
namespace Brackets
{
    class Solution
    {
        private readonly char[] _openingBrackets = new[] {'(', '[', '{'};
        private readonly Dictionary<char, char> _matchingOpenings = new Dictionary<char, char>
        {
            { ')', '(' },
            { ']', '[' },
            { '}', '{' }
        };
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(string S)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var openBrackets = new Stack<char>();
            foreach (var bracket in S)
            {
                if (_openingBrackets.Contains(bracket))
                {
                    openBrackets.Push(bracket);
                }
                else
                {
                    if ((openBrackets.Count == 0) || (openBrackets.Peek() != _matchingOpenings[bracket]))
                    {
                        return 0;
                    }
                    openBrackets.Pop();
                }
            }
            if (openBrackets.Count == 0)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
    }
}