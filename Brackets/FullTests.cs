﻿using NUnit.Framework;

namespace Brackets
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData1()
        {
            Test("{[()()]}", 1);
        }

        [Test]
        public void SampleData2()
        {
            Test("([)()]", 0);
        }

        private void Test(string input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
