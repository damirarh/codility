﻿using System;
using NUnit.Framework;

namespace MaxSliceSum
{
    [TestFixture]
    public class MaxSliceFinderTests
    {
        [Test]
        public void Initialization()
        {
            var finder = new MaxSliceFinder();

            Assert.AreEqual(0, finder.MaxSumEndingHere);
            Assert.AreEqual(Int32.MinValue, finder.MaxSumOverall);
        }

        [Test]
        public void SampleStep1()
        {
            var finder = new MaxSliceFinder();

            finder.Process(3);

            Assert.AreEqual(3, finder.MaxSumEndingHere);
            Assert.AreEqual(3, finder.MaxSumOverall);
        }

        [Test]
        public void SampleStep2()
        {
            var finder = new MaxSliceFinder();
            finder.Process(3);

            finder.Process(2);

            Assert.AreEqual(5, finder.MaxSumEndingHere);
            Assert.AreEqual(5, finder.MaxSumOverall);
        }

        [Test]
        public void SampleStep3()
        {
            var finder = new MaxSliceFinder();
            finder.Process(3);
            finder.Process(2);

            finder.Process(-6);

            Assert.AreEqual(-1, finder.MaxSumEndingHere);
            Assert.AreEqual(5, finder.MaxSumOverall);
        }

        [Test]
        public void SampleStep4()
        {
            var finder = new MaxSliceFinder();
            finder.Process(3);
            finder.Process(2);
            finder.Process(-6);

            finder.Process(4);

            Assert.AreEqual(4, finder.MaxSumEndingHere);
            Assert.AreEqual(5, finder.MaxSumOverall);
        }

        [Test]
        public void SampleStep5()
        {
            var finder = new MaxSliceFinder();
            finder.Process(3);
            finder.Process(2);
            finder.Process(-6);
            finder.Process(4);

            finder.Process(0);

            Assert.AreEqual(4, finder.MaxSumEndingHere);
            Assert.AreEqual(5, finder.MaxSumOverall);
        }
    }
}