﻿using NUnit.Framework;

namespace MaxSliceSum
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] {3, 2, -6, 4, 0}, 5);
        }

        [Test]
        public void ProblematicInput()
        {
            Test(new [] {-10}, -10);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
