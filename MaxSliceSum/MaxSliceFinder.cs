﻿using System;

namespace MaxSliceSum
{
    public class MaxSliceFinder
    {
        public int MaxSumEndingHere { get; private set; }
        public int MaxSumOverall { get; private set; } = Int32.MinValue;

        public void Process(int item)
        {
            if (MaxSumEndingHere < 0)
            {
                MaxSumEndingHere = 0;
            }
            MaxSumEndingHere += item;
            if (MaxSumOverall < MaxSumEndingHere)
            {
                MaxSumOverall = MaxSumEndingHere;
            }
        }
    }
}