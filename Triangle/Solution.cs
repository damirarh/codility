﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/triangle/
namespace Triangle
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            Array.Sort(A);
            for (int i = 0; i < A.Length - 2; i++)
            {
                if ((long)A[i] + A[i + 1] > A[i + 2])
                {
                    return 1;
                }
            }
            return 0;
        }
    }
}