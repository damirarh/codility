﻿using NUnit.Framework;

namespace Triangle
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData1()
        {
            Test(new [] {10, 2, 5, 1, 8, 20}, 1);
        }

        [Test]
        public void SampleData0()
        {
            Test(new [] {10, 50, 5, 1}, 0);
        }

        [Test]
        public void Overflow()
        {
            Test(new [] {int.MaxValue, int.MaxValue, int.MaxValue}, 1);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
