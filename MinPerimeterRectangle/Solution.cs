﻿using System;
using System.Collections.Generic;
using System.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/min_perimeter_rectangle/
namespace MinPerimeterRectangle
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int N)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var divisor = GetDivisors(N).First();
            var pairDivisor = N/divisor;
            return 2 * (divisor + pairDivisor);
        }

        public IEnumerable<int> GetDivisors(int n)
        {
            var maxDivisorToCheck = Convert.ToInt32(Math.Floor(Math.Sqrt(n)));
            for (int candidate = maxDivisorToCheck; candidate > 0; candidate--)
            {
                if (n % candidate == 0)
                {
                    yield return candidate;

                    var pairCandidate = n / candidate;
                    if (candidate != pairCandidate)
                    {
                        yield return pairCandidate;
                    }
                }
            }
        }
    }
}