﻿using System.Security.Policy;
using NUnit.Framework;

namespace MinPerimeterRectangle
{
    [TestFixture]
    public class GetDivisorsTests
    {
        [Test]
        public void Sample()
        {
            var solution = new Solution();

            var expected = new[] { 5, 6, 3, 10, 2, 15, 1, 30};
            var actual = solution.GetDivisors(30);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void DivisorsOf1()
        {
            var solution = new Solution();

            var expected = new[] { 1 };
            var actual = solution.GetDivisors(1);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void DivisorsOf2()
        {
            var solution = new Solution();

            var expected = new[] { 1, 2 };
            var actual = solution.GetDivisors(2);

            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void DivisorsOf3()
        {
            var solution = new Solution();

            var expected = new[] { 1, 3 };
            var actual = solution.GetDivisors(3);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}