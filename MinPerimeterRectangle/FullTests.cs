﻿using NUnit.Framework;

namespace MinPerimeterRectangle
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(30, 22);
        }

        private void Test(int input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
