﻿using System;
using System.Collections;
using System.Collections.Generic;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/peaks/
namespace Peaks
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var peakFinder = new PeakFinder(A);

            var maxGroups = 0;
            foreach (var groupCount in GetDivisors(A.Length))
            {
                if (groupCount < maxGroups)
                {
                    continue;
                }
                if (IsPeakInEveryGroup(peakFinder.Peaks, groupCount))
                {
                    maxGroups = groupCount;
                }
            }

            return maxGroups;
        }

        public IEnumerable<int> GetDivisors(int n)
        {
            var maxDivisorToCheck = Convert.ToInt32(Math.Floor(Math.Sqrt(n)));
            for (int candidate = maxDivisorToCheck; candidate > 0; candidate--)
            {
                if (n % candidate == 0)
                {
                    yield return candidate;

                    var pairCandidate = n / candidate;
                    if (candidate != pairCandidate)
                    {
                        yield return pairCandidate;
                    }
                }
            }
        }

        public bool IsPeakInEveryGroup(BitArray peaks, int groupCount)
        {
            var groupSize = peaks.Length/groupCount;
            var groupIndex = 0;
            var index = 0;
            while (index < peaks.Length)
            {
                if (peaks[index])
                {
                    groupIndex++;
                    index = groupIndex * groupSize;
                }
                else
                {
                    index++;
                    if (index == groupSize * (groupIndex + 1))
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}