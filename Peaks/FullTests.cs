﻿using NUnit.Framework;

namespace Peaks
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new int[] {1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2}, 3);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
