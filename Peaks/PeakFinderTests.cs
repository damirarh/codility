﻿using System.Collections;
using NUnit.Framework;

namespace Peaks
{
    [TestFixture]
    public class PeakFinderTests
    {
        [Test]
        public void Sample()
        {
            var numbers = new int[] {1, 2, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2};
            var finder = new PeakFinder(numbers);

            var expected = new BitArray(numbers.Length, false)
            {
                [3] = true,
                [5] = true,
                [10] = true
            };

            Assert.AreEqual(3, finder.Count);
            Assert.AreEqual(expected, finder.Peaks);
        }
    }
}