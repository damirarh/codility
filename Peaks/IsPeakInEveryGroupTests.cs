﻿using System.Collections;
using NUnit.Framework;

namespace Peaks
{
    [TestFixture]
    public class IsPeakInEveryGroupTests
    {
        private readonly BitArray _peaks = new BitArray(12, false)
        {
            [3] = true,
            [5] = true,
            [10] = true
        };

        [Test]
        public void SampleIn1Group()
        {
            var solution = new Solution();

            var actual = solution.IsPeakInEveryGroup(_peaks, 1);

            Assert.IsTrue(actual);
        }

        [Test]
        public void SampleIn2Groups()
        {
            var solution = new Solution();

            var actual = solution.IsPeakInEveryGroup(_peaks, 2);

            Assert.IsTrue(actual);
        }

        [Test]
        public void SampleIn3Groups()
        {
            var solution = new Solution();

            var actual = solution.IsPeakInEveryGroup(_peaks, 3);

            Assert.IsTrue(actual);
        }

        [Test]
        public void SampleIn4Groups()
        {
            var solution = new Solution();

            var actual = solution.IsPeakInEveryGroup(_peaks, 4);

            Assert.IsFalse(actual);
        }
    }
}