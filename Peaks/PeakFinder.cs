﻿using System.Collections;

namespace Peaks
{
    public class PeakFinder
    {
        public PeakFinder(int[] numbers)
        {
            Peaks = new BitArray(numbers.Length, false);
            for (int i = 1; i < numbers.Length - 1; i++)
            {
                if ((numbers[i-1] < numbers[i]) && (numbers[i+1] < numbers[i]))
                {
                    Peaks[i] = true;
                    Count++;
                }
            }
        }

        public int Count { get; private set; }
        public BitArray Peaks { get; private set; }
    }
}