﻿using NUnit.Framework;

namespace StoneWall
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new [] { 8, 8, 5, 7, 9, 8, 7, 4, 8 }, 7);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
