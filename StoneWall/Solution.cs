﻿using System;
using System.Collections;
using System.Collections.Generic;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/stone_wall/
namespace StoneWall
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] H)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var blockHeights = new Stack<int>();
            var blockCount = 0;
            foreach (var wallHeight in H)
            {
                while ((blockHeights.Count > 0) && (blockHeights.Peek() > wallHeight))
                {
                    blockHeights.Pop();
                }
                if ((blockHeights.Count == 0) || (blockHeights.Peek() < wallHeight))
                {
                    blockHeights.Push(wallHeight);
                    blockCount++;
                }
            }

            return blockCount;
        }
    }
}