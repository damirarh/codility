﻿using System.Collections;
using static System.Math;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/flags/
namespace Flags
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var peakFinder = new PeakFinder(A);
            var nextPeakIndices = GetNextPeakIndices(peakFinder.Peaks);
            var maxFlags = (int)Floor(Sqrt(A.Length)) + 1;

            if (nextPeakIndices[0] == -1)
            {
                return 0;
            }

            var bestFlagCount = 0;
            for (int flagsAvailable = 1; flagsAvailable <= maxFlags; flagsAvailable++)
            {
                var flagCount = 1;
                var nextPeak = nextPeakIndices[0];
                while (flagCount < flagsAvailable)
                {
                    nextPeak += flagsAvailable;
                    if (nextPeak >= A.Length)
                    {
                        break;
                    }
                    nextPeak = nextPeakIndices[nextPeak];
                    if (nextPeak == -1)
                    {
                        break;
                    }
                    flagCount++;
                }
                if (flagCount > bestFlagCount)
                {
                    bestFlagCount = flagCount;
                }
            }

            return bestFlagCount;
        }

        public int[] GetNextPeakIndices(BitArray peaks)
        {
            var nextPeakIndices = new int[peaks.Length];
            var nextPeakIndex = -1;
            for (int i = peaks.Length - 1; i >= 0; i--)
            {
                if (peaks[i])
                {
                    nextPeakIndex = i;
                }
                nextPeakIndices[i] = nextPeakIndex;
            }
            return nextPeakIndices;
        }
    }
}