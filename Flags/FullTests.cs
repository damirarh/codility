﻿using NUnit.Framework;

namespace Flags
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(new int[] {1, 5, 3, 4, 3, 4, 1, 2, 3, 4, 6, 2}, 3);
        }

        [Test]
        public void PackedPeaks()
        {
            Test(new int[] { 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0 }, 4);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
