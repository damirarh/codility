﻿using System.Collections;
using NUnit.Framework;

namespace Flags
{
    [TestFixture]
    public class GetNextPeakIndicesTests
    {
        [Test]
        public void Sample()
        {
            var solution = new Solution();
            var peaks = new BitArray(12, false)
            {
                [1] = true,
                [3] = true,
                [5] = true,
                [10] = true
            };

            var actual = solution.GetNextPeakIndices(peaks);

            var expected = new int[] { 1, 1, 3, 3, 5, 5, 10, 10, 10, 10, 10, -1 };
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}