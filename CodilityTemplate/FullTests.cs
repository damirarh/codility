﻿using NUnit.Framework;

namespace $safeprojectname$
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {

        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
