﻿using NUnit.Framework;

namespace CountSemiprimes
{
    [TestFixture]
    public class CalculateSmallestDivisorsTests
    {
        [Test]
        public void CalculateFor26()
        {
            var solution = new Solution();
            var actual = solution.CalculateSmallestDivisors(26);

            var expected = new[] {0, 0, 0, 0, 2, 0, 2, 0, 2, 3, 2, 0, 2, 0, 2, 3, 2, 0, 2, 0, 2, 3, 2, 0, 2, 5, 2};

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}