﻿using System;
using System.Collections;
using System.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/count_semiprimes/
namespace CountSemiprimes
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int[] solution(int N, int[] P, int[] Q)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var smallestDivisors = CalculateSmallestDivisors(N);
            var semiPrimes = FindSemiPrimes(smallestDivisors);
            var maxUpperBound = Q.Max();
            var smallerSemiPrimeCount = CountSmallerSemiPrimes(semiPrimes, maxUpperBound);

            var semiPrimeCounts = new int[P.Length];
            for (int i = 0; i < P.Length; i++)
            {
                semiPrimeCounts[i] = smallerSemiPrimeCount[Q[i]] - smallerSemiPrimeCount[P[i] - 1];
            }

            return semiPrimeCounts;
        }

        public int[] CalculateSmallestDivisors(int maxNumber)
        {
            var smallestDivisors = new int[maxNumber + 1];
            for (int divisor = 2; divisor <= Math.Floor(Math.Sqrt(maxNumber)); divisor++)
            {
                for (int multiple = divisor*divisor; multiple <= maxNumber; multiple += divisor)
                {
                    if (smallestDivisors[multiple] == 0)
                    {
                        smallestDivisors[multiple] = divisor;
                    }
                }
            }
            return smallestDivisors;
        }

        public BitArray FindSemiPrimes(int[] smallestDivisors)
        {
            var semiPrimes = new BitArray(smallestDivisors.Length, false);
            for (int i = 0; i < smallestDivisors.Length; i++)
            {
                if (smallestDivisors[i] > 0)
                {
                    semiPrimes[i] = smallestDivisors[i/smallestDivisors[i]] == 0;
                }
            }
            return semiPrimes;
        }

        public int[] CountSmallerSemiPrimes(BitArray semiPrimes, int maxNumber)
        {
            var semiPrimeCount = new int[maxNumber + 1];

            for (int i = 1; i <= maxNumber; i++)
            {
                semiPrimeCount[i] = semiPrimeCount[i - 1];
                if (semiPrimes[i])
                {
                    semiPrimeCount[i]++;
                }
            }
            return semiPrimeCount;
        }
    }
}