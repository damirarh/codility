﻿using System.Collections;
using NUnit.Framework;

namespace CountSemiprimes
{
    [TestFixture]
    public class FindSemiPrimesTests
    {
        [Test]
        public void CalculateFor26()
        {
            var smallestDivisors = new[] { 0, 0, 0, 0, 2, 0, 2, 0, 2, 3, 2, 0, 2, 0, 2, 3, 2, 0, 2, 0, 2, 3, 2, 0, 2, 5, 2 };
            var solution = new Solution();

            var actual = solution.FindSemiPrimes(smallestDivisors);
            var expected = new BitArray(27)
            {
                [4] = true,
                [6] = true,
                [9] = true,
                [10] = true,
                [14] = true,
                [15] = true,
                [21] = true,
                [22] = true,
                [25] = true,
                [26] = true
            };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}