﻿using NUnit.Framework;

namespace CountSemiprimes
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(26, new [] {1, 4, 16}, new [] { 26, 10, 20 }, new []{ 10, 4, 0 });
        }

        private void Test(int N, int[] P, int[] Q, int[] expected)
        {
            var solution = new Solution();

            var actual = solution.solution(N, P, Q);

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
