﻿using System.Collections;
using NUnit.Framework;

namespace CountSemiprimes
{
    [TestFixture]
    public class CountSmallerSemiPrimesTests
    {
        [Test]
        public void CalculateFor26()
        {
            var semiPrimes = new BitArray(27)
            {
                [4] = true,
                [6] = true,
                [9] = true,
                [10] = true,
                [14] = true,
                [15] = true,
                [21] = true,
                [22] = true,
                [25] = true,
                [26] = true
            };
            var solution = new Solution();

            var actual = solution.CountSmallerSemiPrimes(semiPrimes, 26);
            var expected = new[] { 0, 0, 0, 0, 1, 1, 2, 2, 2, 3, 4, 4, 4, 4, 5, 6, 6, 6, 6, 6, 6, 7, 8, 8, 8, 9, 10 };

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}