﻿using System;
// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/perm_check
namespace PermCheck
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            try
            {
                var registry = new NumberRegistry(A.Length);
                foreach (var number in A)
                {
                    registry.Register(number);
                }
                return registry.MissingNumbers == 0 ? 1 : 0;
            }
            catch (InvalidOperationException)
            {
                return 0;
            }
        }
    }
}