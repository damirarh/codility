﻿using System;
using NUnit.Framework;

namespace PermCheck
{
    [TestFixture]
    public class NumberRegistryTests
    {
        [Test]
        public void Initialization()
        {
            var registry = new NumberRegistry(5);

            Assert.AreEqual(5, registry.MissingNumbers);
        }

        [Test]
        public void AddFirstValidNumber()
        {
            var registry = new NumberRegistry(5);

            registry.Register(3);

            Assert.AreEqual(4, registry.MissingNumbers);
        }

        [Test]
        public void AddNewValidNumber()
        {
            var registry = new NumberRegistry(5);
            registry.Register(3);

            registry.Register(2);

            Assert.AreEqual(3, registry.MissingNumbers);
        }

        [Test]
        public void AddDuplicateNumber()
        {
            var registry = new NumberRegistry(5);
            registry.Register(3);

            Assert.Throws<InvalidOperationException>(
                () => registry.Register(3));
        }

        [Test]
        public void AddOutOfBoundsNumber()
        {
            var registry = new NumberRegistry(5);

            Assert.Throws<InvalidOperationException>(
                () => registry.Register(6));
        }
    }
}