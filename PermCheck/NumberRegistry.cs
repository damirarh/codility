﻿using System;
using System.Collections;

namespace PermCheck
{
    public class NumberRegistry
    {
        private readonly BitArray _registry;
        public NumberRegistry(int maxValue)
        {
            _registry = new BitArray(maxValue);
            MissingNumbers = maxValue;
        }

        public void Register(int value)
        {
            if (value > _registry.Length)
            {
                throw new InvalidOperationException("Value is out of bounds.");
            }

            int index = value - 1; // numbers are 1-based, registry is 0 based
            if (_registry[index])
            {
                throw new InvalidOperationException("Duplicate value.");
            }
            _registry[index] = true;
            MissingNumbers--;
        }

        public int MissingNumbers { get; private set; }
    }
}