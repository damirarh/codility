﻿using System.Linq;
using NUnit.Framework;

namespace PermCheck
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData1()
        {
            Test(new [] {4, 1, 3, 2}, 1);
        }

        [Test]
        public void SampleData2()
        {
            Test(new [] {4, 1, 3}, 0);
        }

        [Test]
        public void ValueOutOfBounds()
        {
            Test(new [] {2, 1, 3, 4, 12}, 0);
        }

        [Test]
        public void ValueMissing()
        {
            Test(new [] {2, 1, 3, 4, 2}, 0);
        }

        [Test]
        public void LargeInput()
        {
            Test(Enumerable.Range(1, 5000).ToArray(), 1);
        }

        private void Test(int[] input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
