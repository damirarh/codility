﻿using System;
using System.Xml.Linq;

// you can also use other imports, for example:
// using System.Collections.Generic;

// you can write to stdout for debugging purposes, e.g.
// Console.WriteLine("this is a debug message");

// https://codility.com/programmers/task/odd_occurrences_in_array/
namespace OddOccurencesInArray
{
    class Solution
    {
        // ReSharper disable InconsistentNaming Codility requirements
        public int solution(int[] A)
        // ReSharper restore InconsistentNaming
        {
            // write your code in C# 6.0 with .NET 4.5 (Mono)
            var xor = 0;
            foreach (var i in A)
            {
                xor ^= i;
            }

            return xor;
        }
    }
}