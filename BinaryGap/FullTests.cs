﻿using NUnit.Framework;

namespace BinaryGap
{
    [TestFixture]
    public class FullTests
    {
        [Test]
        public void SampleData()
        {
            Test(1041, 5);
        }

        [Test]
        public void TrailingZeros()
        {
            Test(328, 2);
        }

        private void Test(int input, int expected)
        {
            var solution = new Solution();

            var actual = solution.solution(input);

            Assert.AreEqual(expected, actual);
        }
    }
}
