﻿using NUnit.Framework;

namespace BinaryGap
{
    [TestFixture]
    public class BinaryGapFinderTests
    {
        [Test]
        public void Initialization()
        {
            var finder = new BinaryGapFinder(1041);

            Assert.AreEqual(0, finder.DigitsProcessed);
            Assert.AreEqual(1041, finder.RemainingValue);
            Assert.AreEqual(0, finder.CurrentGap);
            Assert.AreEqual(0, finder.MaximumGap);
        }

        [Test]
        public void SampleStep1()
        {
            var finder = new BinaryGapFinder(1041);

            finder.ProcessNextDigit();

            Assert.AreEqual(1, finder.DigitsProcessed);
            Assert.AreEqual(520, finder.RemainingValue);
            Assert.AreEqual(0, finder.CurrentGap);
            Assert.AreEqual(0, finder.MaximumGap);
        }

        [Test]
        public void SampleStep2()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(2, finder.DigitsProcessed);
            Assert.AreEqual(260, finder.RemainingValue);
            Assert.AreEqual(1, finder.CurrentGap);
            Assert.AreEqual(1, finder.MaximumGap);
        }

        [Test]
        public void SampleStep3()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(3, finder.DigitsProcessed);
            Assert.AreEqual(130, finder.RemainingValue);
            Assert.AreEqual(2, finder.CurrentGap);
            Assert.AreEqual(2, finder.MaximumGap);
        }

        [Test]
        public void SampleStep4()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(4, finder.DigitsProcessed);
            Assert.AreEqual(65, finder.RemainingValue);
            Assert.AreEqual(3, finder.CurrentGap);
            Assert.AreEqual(3, finder.MaximumGap);
        }

        [Test]
        public void SampleStep5()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(5, finder.DigitsProcessed);
            Assert.AreEqual(32, finder.RemainingValue);
            Assert.AreEqual(0, finder.CurrentGap);
            Assert.AreEqual(3, finder.MaximumGap);
        }

        [Test]
        public void SampleStep6()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(6, finder.DigitsProcessed);
            Assert.AreEqual(16, finder.RemainingValue);
            Assert.AreEqual(1, finder.CurrentGap);
            Assert.AreEqual(3, finder.MaximumGap);
        }

        [Test]
        public void SampleStep7()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(7, finder.DigitsProcessed);
            Assert.AreEqual(8, finder.RemainingValue);
            Assert.AreEqual(2, finder.CurrentGap);
            Assert.AreEqual(3, finder.MaximumGap);
        }

        [Test]
        public void SampleStep8()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(8, finder.DigitsProcessed);
            Assert.AreEqual(4, finder.RemainingValue);
            Assert.AreEqual(3, finder.CurrentGap);
            Assert.AreEqual(3, finder.MaximumGap);
        }

        [Test]
        public void SampleStep9()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(9, finder.DigitsProcessed);
            Assert.AreEqual(2, finder.RemainingValue);
            Assert.AreEqual(4, finder.CurrentGap);
            Assert.AreEqual(4, finder.MaximumGap);
        }

        [Test]
        public void SampleStep10()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(10, finder.DigitsProcessed);
            Assert.AreEqual(1, finder.RemainingValue);
            Assert.AreEqual(5, finder.CurrentGap);
            Assert.AreEqual(5, finder.MaximumGap);
        }

        [Test]
        public void SampleStep11()
        {
            var finder = new BinaryGapFinder(1041);
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();
            finder.ProcessNextDigit();

            finder.ProcessNextDigit();

            Assert.AreEqual(11, finder.DigitsProcessed);
            Assert.AreEqual(0, finder.RemainingValue);
            Assert.AreEqual(0, finder.CurrentGap);
            Assert.AreEqual(5, finder.MaximumGap);
        }
    }
}