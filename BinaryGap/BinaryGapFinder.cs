﻿namespace BinaryGap
{
    public class BinaryGapFinder
    {
        public int DigitsProcessed { get; private set; }
        public int RemainingValue { get; private set; }
        public int CurrentGap { get; private set; }
        public int MaximumGap { get; private set; }
        public bool OneEncountered { get; set; }

        public BinaryGapFinder(int value)
        {
            RemainingValue = value;
        }

        public void ProcessNextDigit()
        {
            DigitsProcessed++;
            var remainder = RemainingValue%2;
            if (remainder == 0)
            {
                if (OneEncountered)
                {
                    CurrentGap++;
                    if (MaximumGap < CurrentGap)
                    {
                        MaximumGap = CurrentGap;
                    }
                }
            }
            else
            {
                OneEncountered = true;
                CurrentGap = 0;
            }
            RemainingValue /= 2;
        }
    }
}